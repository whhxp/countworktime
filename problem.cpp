#include "problem.h"
#include <cstdio>
#include <iostream>
#include <QProcess>
#include <QString>
#include <qstringlist.h>
#include <QDebug>
#include <time.h>
#include <Windows.h>
#include <QTimer>
#include <QCoreApplication>
Problem::Problem(QObject *parent) : QObject(parent)
{

}

long Problem::evaluate(int sleep_time, double worktime)
{
    printf("----sleep time = %d \n", sleep_time);

    clock_t t_ini,t_fin;
    t_ini=clock();
    double secs;
    QString exePath="delayms.exe";
    QStringList args;
    args.append(QString::number(sleep_time*1000));
    QProcess *maxwell_process = new QProcess;
    QTimer t1;
    bool stopped=false;
    t1.setInterval(10);
    maxwell_process->start(exePath, args);
    connect(maxwell_process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [&stopped](int exitCode, QProcess::ExitStatus exitStatus){ stopped=true; });
    t_ini=clock();
    while(1)
    {
        QCoreApplication::processEvents();
        Sleep(100);
        t_fin = clock();
        secs = (double)(t_fin - t_ini);
        secs = secs / CLOCKS_PER_SEC;
        qDebug()<<"Time: "<<secs<<"secs, Process "<<sleep_time<<maxwell_process->state()<<maxwell_process->exitStatus()<<maxwell_process->exitCode();
        if (worktime == -1)
        {
            if (stopped)
            {
                return sleep_time;
            }
        }
        else
        {
            if(secs > worktime)
            {
                printf("timeout");
                maxwell_process->kill();
                return secs;
            }
        }
    }
//    if (maxwell_process->waitForFinished(-1)){
//        std::cout <<"--------"<< sleep_time << " bat process exit OK" << std::endl;
//        return sleep_time;
//    }else{
//        std::cout <<"--------"<< sleep_time << " bat process exit error" << std::endl;
//        return -1;
//        }
}
