#ifndef PROBLEM_H
#define PROBLEM_H
#include <QObject>

class Problem : public QObject
{
    Q_OBJECT
public:
    explicit Problem(QObject *parent = nullptr);

signals:

public slots:
    long Problem::evaluate(int sleep_time, double worktime);
};

#endif // PROBLEM_H
