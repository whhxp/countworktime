#include <QCoreApplication>
#include <QThread>
#include <QtConcurrent>
#include <iostream>
#include "problem.h"
void concurrent();
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    concurrent();
    return 0;
}

void concurrent()
{
    QThreadPool::globalInstance()->setMaxThreadCount(40);
    int populationSize = 2;
    Problem* problem_ = new Problem();
    QVector<QFuture<long> > futureVector;
    int out;
    QVector<long> timelist;
    for (int i = 0; i < populationSize; ++i)
    {
        QFuture<long> future = QtConcurrent::run(problem_, &Problem::evaluate, i+3,2);
        futureVector.append(future);
    } //for
    std::cout << "init multi thread ok" << std::endl;
    for (int i = 0; i < populationSize; i++)
    {
        QFuture<long> future = futureVector[i];
        future.waitForFinished();
        qDebug()<<future.result();
        timelist.append(future.result());
    }
    std::cout << "****multi thread wait ok****\n****END****" << std::endl;
    delete problem_;
}

